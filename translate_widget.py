from PyQt5.QtWidgets import QWidget,QPushButton
from PyQt5.QtCore import Qt
from translate_config_widget import GuiWidgetTranslateConfig

from image_to_text import ImageToText

class GuiWidgetTranslate(QWidget):
    def __init__(self):
            super().__init__()


    def initUI(self):

        self.imageToText = ImageToText()
        self.setGeometry(100, 100, 200, 200)
        self.setWindowTitle('Select area')
        self.setAutoFillBackground(False)
        self.btn = QPushButton("OK", self)
        self.btn.move(100, 65)
        self.btn.clicked.connect(self.finishSelect)

        self.show()

    def finishSelect(self):
        self.imageToText.start(self.pos(), self.rect())
        self.hide()
