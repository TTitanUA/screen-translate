from PyQt5.QtWidgets import QWidget,QPushButton
from PyQt5.QtCore import Qt
from translate_widget import GuiWidgetTranslate

class GuiWidgetMain(QWidget):
    def __init__(self):
            super().__init__()
            self.initUI()


    def initUI(self):
        self.translateWidget = GuiWidgetTranslate()
        self.setGeometry(100, 100, 600, 600)
        self.setWindowTitle('Screen Translate')

        btn1 = QPushButton("Select area", self)
        btn1.move(100, 65)
        btn1.clicked.connect(self.startSelect)

        self.show()

    def startSelect(self):
        self.translateWidget.initUI()
        self.hide()
        print('test', self)

