from PyQt5.QtWidgets import QWidget,QPushButton,QSpinBox
from PyQt5.QtCore import Qt

class GuiWidgetTranslateConfig(QWidget):
    def __init__(self):
            super().__init__()


    def initUI(self, imageToText):
        self.imageToText = imageToText
        self.setGeometry(100, 100, 200, 200)
        self.setWindowTitle('Translate configuration')
        self.setAutoFillBackground(False)

        self.renderUpdateInterval()
        self.renderFilterMin()
        self.renderFilterMax()

        self.show()

    def renderUpdateInterval(self):
        self.inputInerval = QSpinBox(self)
        self.inputInerval.setValue(self.imageToText.updateInterval)
        self.inputInerval.setDisplayIntegerBase(self.imageToText.updateInterval)
        self.inputInerval.setMinimum(300)
        self.inputInerval.setMaximum(10000)
        self.inputInerval.setSingleStep(100)
        self.inputInerval.move(0, 0)
        self.inputInerval.setPrefix('Update interval: ')
        self.inputInerval.setSuffix(' ms')
        self.inputInerval.valueChanged.connect(self.onUpdateIntervalChange)

    def onUpdateIntervalChange(self):
        print(self.inputInerval.value())
        self.imageToText.setUpdateInterval(self.inputInerval.value())

    def renderFilterMin(self):
        self.inputFilterMin = QSpinBox(self)
        self.inputFilterMin.setValue(self.imageToText.filterMin)
        self.inputFilterMin.setDisplayIntegerBase(self.imageToText.filterMin)
        self.inputFilterMin.setMinimum(0)
        self.inputFilterMin.setMaximum(256)
        self.inputFilterMin.setSingleStep(1)
        self.inputFilterMin.move(0, 20)
        self.inputFilterMin.setPrefix('Filter min: ')
        self.inputFilterMin.valueChanged.connect(self.onFilterMinChange)

    def onFilterMinChange(self):
        print(self.inputFilterMin.value())
        self.imageToText.setFilterMin(self.inputFilterMin.value())

    def renderFilterMax(self):
        self.inputFilterMax = QSpinBox(self)
        self.inputFilterMax.setValue(self.imageToText.filterMax)
        self.inputFilterMax.setDisplayIntegerBase(self.imageToText.filterMax)
        self.inputFilterMax.setMinimum(0)
        self.inputFilterMax.setMaximum(256)
        self.inputFilterMax.setSingleStep(1)
        self.inputFilterMax.move(0, 40)
        self.inputFilterMax.setPrefix('Filter min: ')
        self.inputFilterMax.valueChanged.connect(self.onFilterMaxChange)

    def onFilterMaxChange(self):
        print(self.inputFilterMax.value())
        self.imageToText.setFilterMax(self.inputFilterMax.value())
