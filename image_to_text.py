import pytesseract
import time
import asyncio
from concurrent.futures import ThreadPoolExecutor
import pyperclip
from PIL import Image,ImageGrab
from PyQt5.QtCore import QTimer
from pytesseract import Output

from translate_config_widget import GuiWidgetTranslateConfig

import cv2
import numpy as np

IMAGE_SIZE = 1800
BINARY_THREHOLD = 180

class ImageToText():
    def __init__(self):
        self.translateConfigWidget = GuiWidgetTranslateConfig()
        self.loop = asyncio.get_event_loop()
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0
        self.lastSentence = ''
        pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files\\Tesseract-OCR\\tesseract'
        self.timer = QTimer()
        self.timer.timeout.connect(self.workLoop)
        self.updateInterval = 1000
        self.filterMin = 127
        self.filterMax = 256

    def process(self, image, lang='eng'):
        '''
        OCR Engine modes: --oem
          0    Legacy engine only.
          1    Neural nets LSTM engine only.
          2    Legacy + LSTM engines.
          3    Default, based on what is available.

        Page segmentation modes: --psm
          0    Orientation and script detection (OSD) only.
          1    Automatic page segmentation with OSD.
          2    Automatic page segmentation, but no OSD, or OCR.
          3    Fully automatic page segmentation, but no OSD. (Default)
          4    Assume a single column of text of variable sizes.
          5    Assume a single uniform block of vertically aligned text.
          6    Assume a single uniform block of text.
          7    Treat the image as a single text line.
          8    Treat the image as a single word.
          9    Treat the image as a single word in a circle.
         10    Treat the image as a single character.
         11    Sparse text. Find as much text as possible in no particular order.
         12    Sparse text with OSD.
         13    Raw line. Treat the image as a single text line,
               bypassing hacks that are Tesseract-specific.
        '''
        # custom_config = r'--oem 3 --psm 11'
        return pytesseract.image_to_string(image, lang=lang)

    def start(self, pos, rect):
        self.x = pos.x()
        self.y = pos.y()
        self.width = rect.width()
        self.height = rect.height()
        print('start', rect.width(), rect.height(), pos.x(), pos.y())
        self.stop()

        self.timer.start(self.updateInterval)
        self.translateConfigWidget.initUI(self)
    def stop(self):
        print('stop')
        self.timer.stop()

    def setUpdateInterval(self, value):
        self.updateInterval = value
        self.timer.stop()
        self.timer.start(self.updateInterval)

    def setFilterMin(self, value):
        self.filterMin = value

    def setFilterMax(self, value):
        self.filterMax = value


    def getTimestamp(self):
        return int(round(time.time() * 1000))

    def workLoop(self):
        last_run = 0
        print('loop started')
        # await asyncio.sleep(0.5)
        self.captureImage()
        # while True:
            #curr_time = self.getTimestamp()
            #if curr_time - last_run > 1000:
            #    last_run = curr_time


    def captureImage(self):
        bbox = (self.x, self.y, self.x + self.width, self.y + self.height)
        im = ImageGrab.grab(bbox)
        im = im.convert('RGB')
        im_np = np.array(im)
        im.close()


        im = self.remove_noise_and_smooth(im_np) #old pipe

        #im = self.new_pipeline(im_np) #new pipe
        #cv2.imshow("Screen", im)

        #self.contours_text(im, im_np)

        result = self.process(im, 'jpn').strip()

        if result and result != self.lastSentence:
            #cv2.imwrite("./tmp/" + str(self.getTimestamp()) + ".png", im)
            # cv2.imshow("Screen", im)
            print(result)
            self.lastSentence = result
            pyperclip.copy(result)


    def image_smoothening(self, img):
        ret1, th1 = cv2.threshold(img, BINARY_THREHOLD, 255, cv2.THRESH_BINARY)
        ret2, th2 = cv2.threshold(th1, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        blur = cv2.GaussianBlur(th2, (1, 1), 0)
        ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        return th3

    def remove_noise_and_smooth(self, imgArr):
        #img = cv2.imread(img, 0)
        img = imgArr.astype(np.uint8)
        img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        #filtered = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 3)
        ret,filtered = cv2.threshold(img, 127, 255 , cv2.THRESH_BINARY_INV)
        #kernel = np.ones((1, 1), np.uint8)
        #opening = cv2.morphologyEx(filtered, cv2.MORPH_OPEN, kernel)
        #closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
        #img = self.image_smoothening(img)
        #or_image = cv2.bitwise_or(img, closing)
        #return or_image
        return filtered

    #new pipeline
    def new_pipeline(self, imgArr):
        img = imgArr.astype(np.uint8)
        gray = self.get_grayscale(img)
        thresh = self.thresholding(gray)
        #dilate = self.dilate(gray)
        # erode = self.erode(gray)
        # opening = self.opening(gray)
        # canny = self.canny(gray)
        return thresh

    def get_grayscale(self, image):
        return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # noise removal
    def remove_noise(self, image):
        return cv2.blur(image, 3)
        return cv2.medianBlur(image, 2)

    # thresholding
    def thresholding(self, image):
        # threshold the image, setting all foreground pixels to
        # 255 and all background pixels to 0

        # cv2.imshow("THRESH_BINARY", cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)[1])
        return cv2.threshold(image, self.filterMin, self.filterMax, cv2.THRESH_BINARY_INV)[1]

        #cv2.imshow("ADAPTIVE_THRESH_GAUSSIAN_C", cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 11))


        #return cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    # dilation
    def dilate(self, image):
        kernel = np.ones((5, 5), np.uint8)
        return cv2.dilate(image, kernel, iterations=1)

    # erosion
    def erode(self, image):
        kernel = np.ones((5, 5), np.uint8)
        return cv2.erode(image, kernel, iterations=1)

    # opening - erosion followed by dilation
    def opening(self, image):
        kernel = np.ones((5, 5), np.uint8)
        return cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)

    # canny edge detection
    def canny(self, image):
        return cv2.Canny(image, 100, 200)

    def contours_text(self, image, orig):
        config = ('-l jpn --oem 1 --psm 3')
        d = pytesseract.image_to_data(image, output_type=Output.DICT, config=config)
        n_boxes = len(d['level'])
        print(n_boxes)
        for i in range(n_boxes):
            (x, y, w, h) = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])
            cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 255, 0), 2)
            print(d)
        cv2.imshow('img', orig)
        cv2.waitKey(0)

        # contours, _ = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        # contours, _ = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_TC89_KCOS)
        # contours, hierarchy = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        #
        # for contour in contours:
        #     approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
        #     print(approx)
        #     cv2.drawContours(orig, [approx], -1, (0, 255, 255), 2)
        #     cv2.imshow('cnt', orig)
        #     cv2.waitKey()

        # for cnt in contours:
        #     x, y, w, h = cv2.boundingRect(cnt)
        #
        #     # Drawing a rectangle on copied image
        #     rect = cv2.rectangle(orig, (x, y), (x + w, y + h), (0, 0, 255), 2)
        #
        #     print(x, y, w, h)
        #
        #     # Cropping the text block for giving input to OCR
        #     cropped = image[y:y + h, x:x + w]
        #
        #     # Apply OCR on the cropped image
        #     config = ('-l jpn --oem 1 --psm 3')
        #     text = pytesseract.image_to_string(cropped, config=config)
        #
        #     print(text)