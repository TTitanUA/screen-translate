# screen-translate

## requirements
https://digi.bib.uni-mannheim.de/tesseract/ - tesseract 4

https://pillow.readthedocs.io/en/latest/installation.html - Pillow

https://pypi.org/project/pytesseract/ - PyTesseract

https://pypi.org/project/PyQt5/ - PyQt5 GUI
